var s_res = 0//默认皮肤
var s_title = "h5 App应用演示";

// 动态加载css文件
function loadStyles(url) {
	var link = document.createElement("link");
	link.type = "text/css";
	link.rel = "stylesheet";
	link.href = url;
	document.getElementsByTagName("head")[0].appendChild(link);
	document.title=s_title;
}

// 动态加载js脚本文件
function loadScript(url) {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = url;
	document.body.appendChild(script);
}

if(s_res == 0)
{
	loadStyles("../css_01/fonts-google.css");
    loadStyles("../css_01/font-awesome.min.css");
    loadStyles("../css_01/animate.css");
    loadStyles("../css_01/cryptocoins.css");
    loadStyles("../css_01/plugins/c3-chart/c3.css");
    loadStyles("../css_01/global.style.css");
    loadStyles("../css_01/swiper.min.css");
    loadStyles("../css_01/jqcandlestick.css");
    loadStyles("../css_01/bootstrap.min.css");
    loadStyles("../css_01/plugins/turbo-slider/turbo.css");
	loadStyles("../css_01/plugins/sweetalert/sweetalert.css");
}
else
{
	loadStyles("../css_02/fonts-google.css");
    loadStyles("../css_02/font-awesome.min.css");
    loadStyles("../css_02/animate.css");
    loadStyles("../css_02/cryptocoins.css");
    loadStyles("../css_02/plugins/c3-chart/c3.css");
    loadStyles("../css_02/global.style.css");
    loadStyles("../css_02/swiper.min.css");
    loadStyles("../css_02/jqcandlestick.css");
    loadStyles("../css_02/bootstrap.min.css");
    loadStyles("../css_02/plugins/turbo-slider/turbo.css");
	loadStyles("../css_02/plugins/sweetalert/sweetalert.css");
}