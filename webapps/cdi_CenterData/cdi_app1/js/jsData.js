var tagSelectOptions_json = {"tagSelectOptions":[{"main_id":"1","s_name":"普通用户"},{"main_id":"2","s_name":"运维用户"},{"main_id":"3","s_name":"管理员"},{"main_id":"4","s_name":"超级管理员"}]};

var tagAB001_json = {"tagAB001":[{"img_src":"../img_01/content/icon1.png","s_name":"发送"},{"img_src":"../img_01/content/icon2.png","s_name":"接收"},{"img_src":"../img_01/content/icon3.png","s_name":"提供"},{"img_src":"../img_01/content/icon4.png","s_name":"加满"}]};

var tagSResultList_json = {"tagSResultList":[{"msg_state":"notice-icon available","result_info":"数据查询成功","msg_time":"15分钟前","msg_icon":"fa fa-check"},
											 {"msg_state":"notice-icon away","result_info":"数据更新成功","msg_time":"12小时前","msg_icon":"fa fa-clock-o"},
											 {"msg_state":"notice-icon busy","result_info":"系统设置成功","msg_time":"3天前","msg_icon":"fa fa-times"},
											 {"msg_state":"notice-icon available","result_info":"数据设置成功","msg_time":"1个月前","msg_icon":"fa fa-check"}]};
											 
var tagAA001_json = {"tagAA001":[{"money_sum":"¥2,589.50","money_desc":"账户总金额","url_path":"../account/profile.html","user_img":"../img_01/content/avatar/avatar.png"}]};

var tagAC001_json = {"tagAC001":[{"ims_src":"../img_01/content/coin1.png","money_name":"人民币","sys_date":"08-24","sys_time":"20.04PM","value_add":"+0.94853","num_value":"¥2,748.9"},
							   {"ims_src":"../img_01/content/coin4.png","money_name":"美元","sys_date":"10-11","sys_time":"10.35AM","value_add":"-0.343","num_value":"$239.94"}]};

var tagAE001_json = {"tagAE001":[{"ims_src":"../img_01/content/ex2.png","income_type":"总收入","money_value":"¥2,748,98"},
							   {"ims_src":"../img_01/content/ex1.png","income_type":"总支出","money_value":"¥1,643.22"}]};
							   
var tagAG001_json = {"tagAG001":[{"ims_src":"../img_01/content/ex2.png","income_type":"张三","sel_value":"1"},
							   {"ims_src":"../img_01/content/ex1.png","income_type":"李四","sel_value":"3"},
							   {"ims_src":"../img_01/content/ex2.png","income_type":"孙无","sel_value":"2"},
							   {"ims_src":"../img_01/content/ex1.png","income_type":"赵刘","sel_value":"1"}]};