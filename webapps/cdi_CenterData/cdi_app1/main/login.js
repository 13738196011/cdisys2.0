var param_name1 = 'A01_CG3NLW';
var param_name2 = 'T01_weixin_login';
var param_value1 = "";
var param_value2 = "";
var param_value3 = "";
var param_value4 = "";
var param_value5 = "";
var openid = "";
var img_url = "";
var weixin_nickname = "";

/*页面业务逻辑开始*/
function biz_start()
{
	//alert('profile true');
}

$(document).ready(function () {	
	//添加一个获取参数的函数
	(function ($) {
		$.getUrlParam = function (name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)")
			var r = window.location.search.substr(1).match(reg)
			if (r != null) return unescape(r[2])
			return null
		}
	})(jQuery)
	//name 和 age即是微信小程序传递过来的参数
	openid = $.getUrlParam('openid');
	img_url = $.getUrlParam('img_url');
	weixin_nickname = $.getUrlParam('weixin_nickname');
	//alert(window.location.href);
	//swal("openid"+openid,"img_url"+img_url+"weixin_nickname"+weixin_nickname);
	if(openid != null && openid != "null" && openid != "" && typeof(openid) != "undefined"){
		var inputdata = {param_name: param_name1,param_value1: "guest",param_value2: "1"};
		get_ajax_baseurl(inputdata,"login_result")
	}
	var inputdata = {param_name:"T01_sel_t_app_menu_bottom"};	
	get_ajax_baseurl(inputdata,"init_result")
});

function login(){
	var inputdata = {param_name: param_name1,param_value1: $('#txtAccount').val(),param_value2: $('#txtPwd').val()};
	get_ajax_baseurl(inputdata,"login_result")
}

function login_result(input)
{
	layer.close(ly_index);
	data2 = input.A01_CG3NLW;
	var s_result = "";
	var error_desc = "";
	for (var key in data2[0]) {
		if (key == 's_result') 
		{
			s_result = data2[0].s_result;
			error_desc = data2[0].error_desc;
		}
	}
	if (s_result != "1") {
		swal("登录失败！", "登录失败！:"+error_desc, "warning");
	}
	else{
		localStorage.setItem('session_id', data2[0].session_id);				
		localStorage.setItem('login_id', data2[0].MAIN_ID);			
		localStorage.setItem('nickname', data2[0].nickname);	
		localStorage.setItem('rid', data2[0].rid);
		
		localStorage.setItem('openid', openid);
		localStorage.setItem('img_url', img_url);
		localStorage.setItem('weixin_nickname', weixin_nickname);
		window.location.href = "../index/index.html";
	}
}

function weixin_login(){
	//swal("openid",openid+img_url+weixin_nickname);
	if(openid == null || openid == "null" || openid == "" || typeof(openid) == "undefined"){
		wx.miniProgram.navigateTo({url: "/pages/login/login"});
	}
	else{
		//var inputdata = {param_name: param_name1,param_value1: "guest",param_value2: "1"};
		//get_ajax_baseurl(inputdata,"login_result")
		//wx.miniProgram.navigateTo({url: "/pages/index/index?openid="+openid+"&img_url="+img_url+"&weixin_nickname="+weixin_nickname});
		swal("openid:"+openid,"用户已登陆微信，请先退出!")
		//swal("openid"+openid,"img_url"+img_url+"weixin_nickname"+weixin_nickname+"用户已登陆微信，请先退出!")
	}
	//var inputdata = {param_name: param_name2,param_value1: "wx6c6639caef9be661",param_value2: "e7aaa4ef66eace53c03a4945f6c3611a",param_value3: openid};
	//get_ajax_baseurl(inputdata,"weixin_login_result")
}

function weixin_login_result(input)
{
	layer.close(ly_index);
	data2 = input.T01_weixin_login;
	var s_result = "";
	var error_desc = "";
	for (var key in data2[0]) {
		if (key == 's_result') 
		{
			s_result = data2[0].s_result;
			error_desc = data2[0].error_desc;
		}
	}
	if (s_result != "1") {
		swal("微信登录失败！", "微信登录失败！:"+error_desc, "warning");
	}
	else{	
		localStorage.setItem('session_key', data2[0].session_key);	
		localStorage.setItem('openid', data2[0].openid);
		window.location.href = "../index/index.html";
	}
}