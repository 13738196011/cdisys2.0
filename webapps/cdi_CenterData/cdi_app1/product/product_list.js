// 加载select的option objKey为option value objKeyCol为记录中下拉框ID
/*
function tagSelect_get_init(objKey,objKeyCol)
{
	//设置select的id和name
	tagSelect_template = tagSelect_template.replace(new RegExp("$1001","gm"),"sel_ent_role_"+objKey.toString());
	var objhtml=$(tagSelect_template);
	
	//加载select的options
	data = tagSelectOptions_json.tagSelectOptions;
    $.each(data,function (i, obj)
    {	
		//选中option
		if(objKey == obj[objKeyCol])
		{
			obj["select_value"]="selected";			
			var template_temp = set_template(tagSelectOptions_template,"tagSelectOptions_template",obj,"tagSelectOptions");
			objhtml = $(objhtml).append(template_temp);
		}
		else
		{
			obj["select_value"]="";
			var template_temp = set_template(tagSelectOptions_template,"tagSelectOptions_template",obj,"tagSelectOptions");
			objhtml = $(objhtml).append(template_temp);
		}
    });
	return objhtml;
}
*/
/*加载信息中含有select等组件的模板
function tagXXX_get(input)
{
	data = input.A01_T3HK8;
	$.each(data,function (i, obj){
			if(i == 0)
			{
				//模板对象(参见template_tag.js);模板名字符串(参见TemplateIFaceLink.json);数据对象;接口返回对象名字符串(参见jsData.js)
				var template_temp = $(set_template(tagAE001_template,"tagAG001_template",obj,"tagAG001"));	
				//加载select模板对象
				var temselect = tagSelect_get_init(obj["sel_value"],"main_id");			 
				template_temp.find("h4:contains('$1003')").append(temselect);
				var th = $(template_temp).html();			
				$("#tagAG001").append(th.replaceAll("$1003",""));
			}
			else
			{
				//模板对象(参见template_tag.js);模板名字符串(参见TemplateIFaceLink.json);数据对象;接口返回对象名字符串(参见jsData.js)
				var template_temp = $(set_template(tagAE001_template1,"tagAG001_template1",obj,"tagAG001"));
				//加载select模板对象
				var temselect = tagSelect_get_init(obj["sel_value"],"main_id");
				template_temp.find("h4:contains('$1003')").append(temselect);
				var th = $(template_temp).html();
				$("#tagAG001").append(th.replaceAll("$1003",""));
			}
	    });
}
*/

//调用ajax后台接口并回调函数
//var inputdata = {param_name:"接口名称"};
//get_ajax_baseurl(inputdata,"回调函数")

//java调用js函数
/*
function use_Js_function(objParameter)
{
	aler(objParameter);
	$("#txtJavaResult").text(objParameter);
}
*/
//调用后台java函数
/*
function use_java_function(iIndex)
{
	if(typeof(InterfaceName ) != "undefined")
	{
		var java_fun = "";
		//iIndex 1 :访问通讯录录 2:文件夹及路径
		java_fun = InterfaceName.test_fun(iIndex);
		alert(java_fun);
		$("#txtJavaResult").text(java_fun);
	}
	else{
		alert("run failure!")
	}
}
*/
var APP_USER_ZXY = {};
var objparam_value1 = getUrlParam("objparam_value1");
var objparam_value2 = getUrlParam("objparam_value2");
var objparam_value3 = getUrlParam("objparam_value3");
var objparam_value4 = getUrlParam("objparam_value4");
var objparam_value5 = getUrlParam("objparam_value5");
var objparam_value6 = getUrlParam("objparam_value6");
var objparam_value7 = getUrlParam("objparam_value7");
var objparam_value8 = getUrlParam("objparam_value8");
var objparam_value9 = getUrlParam("objparam_value9");
var objparam_value10 = getUrlParam("objparam_value10");

//业务逻辑数据开始
function biz_start()
{
	/*biz begin*/	
	//请传入对应接口参数
	if(jsonReadCommonRes != null && typeof(jsonReadCommonRes) != "undefined" && jsonReadCommonRes.hasOwnProperty("Get_APP_USER_ZXY"))
	{
		Get_APP_USER_ZXY(jsonReadCommonRes);
	}
	else
	{
		var inputdata = {"param_name":"APP_USER_ZXY","session_id":session_id,"login_id":login_id,"param_value1":"EHK0203BCD05"};
		get_ajax_baseurl(inputdata,"Get_APP_USER_ZXY");
	}
	/*biz end*/
}

/*biz step begin*/
function Get_APP_USER_ZXY(data){
	if(data != null)
		APP_USER_ZXY = data;
	if(jsonReadCommonRes != null && typeof(jsonReadCommonRes) != "undefined" && jsonReadCommonRes.hasOwnProperty("A01_A00004"))
	{
		A01_A00004_0(jsonReadCommonRes);
	}
	else if(APP_USER_ZXY != null && typeof(APP_USER_ZXY) != "undefined" && APP_USER_ZXY.hasOwnProperty("A01_A00004"))
	{
		A01_A00004_0(APP_USER_ZXY);
	}
	else
	{
		//请传入对应接口参数
		var inputdata = {"param_name":"A01_A00004","session_id":session_id,"login_id":login_id};
		get_ajax_baseurl(inputdata,"A01_A00004_0");
	}
}

function A01_A00004_0(input){
	data = input.A01_A00004;
	var AAA_PATH = "";
	var s_result = "1";
	var error_desc = "";
	for (var key in data[0]) {
		if (key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if (s_result == "0") {
		swal("获取数据异常！", "获取数据异常！:A01_A00004"+error_desc, "warning");
	}
	else
	{
		$.each(data,function (i, obj)
		{
			var template_temp = set_template(tagAAAC001_template,"tagAAAC001_template",obj,"A01_A00004").replace("$AAA",AAA_PATH);
			$("#tagAAAC001").append(template_temp);
		});
	}
	layer.close(ly_index);
	if(jsonReadCommonRes != null && typeof(jsonReadCommonRes) != "undefined" && jsonReadCommonRes.hasOwnProperty("A01_A00001"))
	{
		A01_A00001_1(jsonReadCommonRes);
	}
	else if(APP_USER_ZXY != null && typeof(APP_USER_ZXY) != "undefined" && APP_USER_ZXY.hasOwnProperty("A01_A00001"))
	{
		A01_A00001_1(APP_USER_ZXY);
	}
	else
	{
		//请传入对应接口参数
		var inputdata = {"param_name":"A01_A00001","session_id":session_id,"login_id":login_id};
		get_ajax_baseurl(inputdata,"A01_A00001_1");
	}
}

function A01_A00001_1(input){
	data = input.A01_A00001;
	var AAA_PATH = "";
	var s_result = "1";
	var error_desc = "";
	for (var key in data[0]) {
		if (key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if (s_result == "0") {
		swal("获取数据异常！", "获取数据异常！:A01_A00001"+error_desc, "warning");
	}
	else
	{
		$.each(data,function (i, obj)
		{
			var template_temp = set_template(tagAAAE001_template,"tagAAAE001_template",obj,"A01_A00001").replace("$AAA",AAA_PATH);
			$("#tagAAAE001").append(template_temp);
		});
	}
	layer.close(ly_index);
}

/*biz step end*/

$(document).ready(function () {
	//页面初始化
	//init_page();
});

window.onpageshow = function(e){
	if(e.persisted || (window.performance.navigation.type == 2)){
		is_history_back = 1;
	}
	else{
		is_history_back = 0;
	}
	//页面初始化
	init_page();
};
