﻿var iTop = 0;
var ileft = 0;
var ly_index;

var options = "";
var set_year = "";
//药剂添加量
var t_sub_sys=[];

function A01_65T431(input)
{
	$.each(input.A01_65T431,function(i, obj) {
		if(i != input.A01_65T431.length - 1) 
		{
 			options += obj.MAIN_ID + ":" + obj.NAME + ";";
 		} else {
 			options += obj.MAIN_ID + ":" + obj.NAME;
 		}
	});
	
	t_sub_sys=[
	        {label:'主键',name:'MAIN_ID',width:'30px',index:'MAIN_ID',editable: false,key:true,readOnly:true,editrules : { required: true}}
	        ,{label:'因素名称',name:'ELEMENT_ID',width:'70px',edittype:'select',formatter:'select',editable:true,editrules:true,editoptions: {value:{79:'药剂添加量',91:'钙离子的含量'}},formatoptions:{value:{79:'药剂添加量',91:'钙离子的含量'}}}
	       	,{label:'机组名称',name:'STATION_TYPE',width:'70px',editable: true,readOnly:true,editrules:true,edittype:'select',editoptions: {value:options}}
	       	,{label:'添加时间',name:'GET_TIME',width:'70px',editable:true,editrules:true,readOnly:true,formatter:function(value,row){return new Date(value).Format('yyyy-MM-dd hh:mm:ss');},editoptions:{defaultValue:new Date().Format('yyyy-MM-dd hh:mm:ss')}}
	       	,{label:'数据值',name:'COND_VALUE',width:'70px',editable:true,editrules:true}
	       	,{label:'备注',name:'S_DESC',width:'70px',editable:true,editrules:true}
	       	,{label:'系统时间',name:'CREATE_DATE',width:'70px',editable:true,editrules:true,readOnly:true,formatter:function(value,row){return new Date(value).Format('yyyy-MM-dd hh:mm:ss');},editoptions:{defaultValue:new Date().Format('yyyy-MM-dd hh:mm:ss')}}
	       ];
	$("#set_year").val(new Date().getFullYear());
	QryData();
}

function QryData()
{
	if($("#set_year").val() != "")
	{
		set_year = $("#set_year").val();
		init();
	}
	else
	{
		swal({
			title: "告警",
			text: "年份不能为空,请重新输入！",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "刷新",
			closeOnConfirm: false
		});
	}
}

//获取机组编号
function get_A01_65T431() {
	ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "A01_65T431",
			session_id:session_id,
			param_value1:"F"
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "A01_65T431",
        success: function(data2) {
		},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			})
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function A01_GT9H6T(input)
{
	data = input.A01_GT9H6T;
	var total = $("#table_list_1").jqGrid('getGridParam', 'records')
	if(typeof(total) == "undefined")
	{
		$("#table_list_1").jqGrid({
			data: data,
			datatype: "local",
			height: "300",
			autowidth: true,
			shrinkToFit: true,
			rowNum: 10,
			rowList: [10, 20, 30],
			colModel: t_sub_sys,
			editurl: 'clientArray',
			altRows: true,
			pager: "#pager_list_1",
			viewrecords: true,
			caption: "药剂添加量信息列表",
			gridview:true
		});
		
		//导航后面，增加增删改查按钮
		$("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
			edit: true,
			add: true,
			del: true,
			search: true,
			view: false,
			position: "left",
			cloneToTop: false
		},
		{
			beforeShowForm: function() {
				$('#MAIN_ID').attr('readOnly', true);
				laydate.render({
					elem: '#GET_TIME',
					type: 'datetime'
				});
				laydate.render({
					elem: '#CREATE_DATE',
					type: 'datetime'
				});
			},
			onclickSubmit: updateDbByMainId,
			closeAfterEdit: true
		},
		{
			beforeShowForm: function() {
				laydate.render({
					elem: '#GET_TIME',
					type: 'datetime'
				});			
				laydate.render({
					elem: '#CREATE_DATE',
					type: 'datetime'
				});
			},
			onclickSubmit: insertDb,
			closeAfterAdd: true
		},
		{
			top: iTop,
			left: ileft,
			onclickSubmit: delDbByMainId
		},
		{
			closeAfterSearch: true
		},
		{
			height: 200,
			reloadAfterSubmit: true
		});
	}
	else
	{
		$("#table_list_1").jqGrid("clearGridData");
		$("#table_list_1").jqGrid('setGridParam',{
			data: data
		}).trigger("reloadGrid");
	}
	layer.close(ly_index);
}

//初始化
function init() {
	ly_index = layer.load();
	iTop = (winHeight - 300) / 2;
	ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
	$("#table_list_1").jqGrid("clearGridData");
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "A01_GT9H6T",
			session_id:session_id,
			param_value1:set_year
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "A01_GT9H6T",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function() {
	//init_page();
    getWindowSize();
    get_A01_65T431();
    $(window).bind("resize",
    function() {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function Ins_A01_GT9H6T(input)
{
	data2 = input.Ins_A01_GT9H6T;
	var s_result = "";
	var error_desc = "";
	for (var key in data2[0]) {
		if (key == 's_result') 
		{
			s_result = data2[0].s_result;
			error_desc = data2[0].error_desc;
		}
	}
	if (s_result != "1") {
		swal("操作失败!", "失败原因:"+error_desc, "warning");
	}
	else{
		swal("操作成功!", "", "success");
	}
	layer.close(ly_index);
	init();
}

var insertDb = function() {
	ly_index = layer.load();
	$.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "Ins_A01_GT9H6T",
			session_id:session_id,
			param_value1: $("#ELEMENT_ID").val(),
			param_value2: $("#STATION_TYPE").val(),
			param_value3: $("#GET_TIME").val(),
			param_value4: $("#COND_VALUE").val(),
			param_value5: s_encode($("#S_DESC").val())	
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "Ins_A01_GT9H6T",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function Upd_A01_GT9H6T(input)
{
	data2 = input.Upd_A01_GT9H6T;
	var s_result = "";
	var error_desc = "";
	for (var key in data2[0]) {
		if (key == 's_result') 
		{
			s_result = data2[0].s_result;
			error_desc = data2[0].error_desc;
		}
	}
	if (s_result != "1") {
		swal("操作失败!", "失败原因:"+error_desc, "warning");
	}
	else{
		swal("操作成功!", "", "success");
	}
	layer.close(ly_index);
	init();
}

var updateDbByMainId = function() {
	var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
	var rowData = $('#table_list_1').jqGrid('getRowData', rowid).MAIN_ID;
	if(rowid.indexOf("jqg") != -1 || rowData.indexOf("jqg") != -1)
	{
		swal({
			title: "提示信息",
			text: "无法修改缓存记录，请选择正确修改!"
		});
		return false;
	}
	ly_index = layer.load();
	$.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "Upd_A01_GT9H6T",
			session_id:session_id,
			param_value1: rowData,
			param_value2: $("#ELEMENT_ID").val(),
			param_value3: $("#STATION_TYPE").val(),
			param_value4: $("#GET_TIME").val(),
			param_value5: $("#COND_VALUE").val(),
			param_value6: s_encode($("#S_DESC").val())	
        },			
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "Upd_A01_GT9H6T",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function Del_A01_GT9H6T(input)
{
	data2 = input.Del_A01_GT9H6T;
	var s_result = "";
	var error_desc = "";
	for (var key in data2[0]) {
		if (key == 's_result') 
		{
			s_result = data2[0].s_result;
			error_desc = data2[0].error_desc;
		}
	}
	if (s_result != "1") {
		swal("操作失败!", "失败原因:"+error_desc, "warning");
	}
	else{
		swal("操作成功!", "", "success");
	}
	layer.close(ly_index);
	init();
    //$("#table_list_1").trigger("reloadGrid");
}

var delDbByMainId = function() {	
	var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
	var rowData = $('#table_list_1').jqGrid('getRowData', rowid).MAIN_ID;
	var rowGetTime = $('#table_list_1').jqGrid('getRowData', rowid).GET_TIME;
	if(rowid.indexOf("jqg") != -1 || rowData.indexOf("jqg") != -1)
	{
		swal({
			title: "提示信息",
			text: "无法删除缓存记录，请选择正确删除!"
		});
		return false;
	}
	ly_index = layer.load();
	$.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
		data:{
            param_name: "Del_A01_GT9H6T",
			session_id:session_id,
			param_value1: rowData,	
			param_value2: rowGetTime		
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "Del_A01_GT9H6T",
        success: function() {},
        error: function() {
			layer.close(ly_index);
			swal({
				title: "告警",
				text: "网络异常或系统故障，请刷新页面！",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "刷新",
				closeOnConfirm: false
			},
			function() {
				window.location.reload();
			});
		},
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
		complete: function(XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};