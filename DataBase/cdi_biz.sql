CREATE DATABASE  IF NOT EXISTS `cdi_biz` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cdi_biz`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cdi_biz
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `courseware`
--

DROP TABLE IF EXISTS `courseware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseware` (
  `main_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `content` text,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `count` int(11) DEFAULT '0',
  `cover_url` varchar(500) NOT NULL,
  `file_url` varchar(500) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`main_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courseware`
--

LOCK TABLES `courseware` WRITE;
/*!40000 ALTER TABLE `courseware` DISABLE KEYS */;
INSERT INTO `courseware` VALUES (1,'H5模板代码一键生成器','笔者作为多年的资深开发，深知调试前端UI是后端开发人员最惧怕的事，并且厌倦了无止境的Ctrl+C和Ctrl+V工作了，最近闲暇时光，想想能不能把一些繁杂的体力活(Ctrl+C和Ctrl+V)给释放出来？baidu了下发现网上h5代码一键生成器有很多但能同时绑定业务数据及前端展现的功能框架少之又少，于是花了2周时间研究了这个h5代码一键生成，实现同步绑定业务数据及前端UI模板自动生成页面功能',100.00,99,'1.png','/a.docx','2021-02-18 05:55:11'),(2,'监测数据采集物联网应用解决方案','本方案主要应用于数据采集、传输、及平台展示一体化解决方案中软件技术框架部分。本方案软件技术框架具有以下特点:开发时间短、效率高、兼容性强、部署简单。本方案软件技术框架涉及两大部分：平台接收及数据展示和物联网设备数据采集端；',100.00,99,'2.png','/b.docx','2021-02-18 05:55:11'),(3,'通用数据接口管理系统CDI介绍','本系统作为关系型数据库接口配置管理系统，可通过图形化界面配置各类数据接口，无需编写任何后台java代码，实现可视化配置即结果模式，易于扩展；可将程序开发人员的重复接口定义工作从繁琐的重复性后台代码中解脱出来，让程序开发人员更专注于数据业务的分析和理解。使用该系统人员需要对数据库具有一定应用基础知识，熟练掌握各类数据库SQL语句及存储过程编写能力。',100.00,99,'3.png','/c.docx','2021-02-20 13:56:14'),(4,'',NULL,0.00,0,' ','','2021-04-19 11:33:16');
/*!40000 ALTER TABLE `courseware` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ent_list`
--

DROP TABLE IF EXISTS `ent_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ent_list` (
  `MAIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENT_CODE` varchar(50) DEFAULT NULL,
  `ENT_NAME` varchar(200) DEFAULT NULL,
  `REGIONCODE` varchar(200) DEFAULT NULL,
  `URL_PATH` varchar(500) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MAIN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ent_list`
--

LOCK TABLES `ent_list` WRITE;
/*!40000 ALTER TABLE `ent_list` DISABLE KEYS */;
INSERT INTO `ent_list` VALUES (1,'001','百度公司','A001','https://www.baidu.com/img/flexible/logo/pc/result.png','2021-01-08 15:49:15'),(2,'002','顺丰快递','A002','https://www.sf-express.com/resource/images/index/sf.png','2021-01-08 15:50:32'),(3,'003','瑞旭科技','A003','https://www.cirs-group.com/Public/assets/images/logo.svg','2021-01-08 15:51:24'),(4,'004','光谷技术','A004','http://www.ggjs.cc/static/content/images/logo.jpg','2021-01-08 15:51:55'),(5,'005','太阳技术','A005','http://www.hzsun.com.cn/images/logo.jpg','2021-01-08 15:52:34'),(6,'006','金科文化','A006','https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1422008071,1765721399&fm=26&gp=0.jpg','2021-01-08 19:10:37');
/*!40000 ALTER TABLE `ent_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ent_user_list`
--

DROP TABLE IF EXISTS `ent_user_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ent_user_list` (
  `MAIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENT_ID` varchar(50) DEFAULT NULL,
  `USER_NAME` varchar(200) DEFAULT NULL,
  `USER_AGE` int(11) DEFAULT NULL,
  `USER_TEL` varchar(200) DEFAULT NULL,
  `URL_PATH` varchar(500) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MAIN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ent_user_list`
--

LOCK TABLES `ent_user_list` WRITE;
/*!40000 ALTER TABLE `ent_user_list` DISABLE KEYS */;
INSERT INTO `ent_user_list` VALUES (1,'1','孙天',32,'13733733733','https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3126544359,3032901862&fm=26&gp=0.jpg','2021-01-08 17:18:24'),(2,'1','张可专',25,'13933393911','https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1513337857,4102385082&fm=15&gp=0.jpg','2021-01-08 17:19:08'),(3,'6','王建',0,'1331155161','https://aiqicha.baidu.com/person?personId=a74b82804b2cd711e988147a2bdbab41','2021-01-08 19:11:30'),(4,'5','陈天乐',32,'1223131441','https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3255515337,647325698&fm=26&gp=0.jpg','2021-01-11 10:38:46'),(5,'4','张三',45,'14789271912','https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3206336446,4025019069&fm=26&gp=0.jpg','2021-01-11 10:39:58'),(6,'3','李四',54,'12324567312','https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=144720555,4207875137&fm=26&gp=0.jpg','2021-01-11 10:40:53'),(7,'2','王五',44,'12345667832','https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1574090977,2717839684&fm=26&gp=0.jpg','2021-01-11 10:41:18');
/*!40000 ALTER TABLE `ent_user_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `index_page`
--

DROP TABLE IF EXISTS `index_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `index_page` (
  `main_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` varchar(5000) DEFAULT NULL,
  `img_url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`main_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `index_page`
--

LOCK TABLES `index_page` WRITE;
/*!40000 ALTER TABLE `index_page` DISABLE KEYS */;
INSERT INTO `index_page` VALUES (1,'0后端代码快速开发工具','<p>本应用由H5模板代码一键生成器生成界面</p><p>由通用数据接口管理系统CDI图形化生成数据接口</p><p>适合手机端H5小程序和APP快速开发</p><p>简单！！！快速！！！好用！！！</p><p>演示账号地址：</p><p>http://124.71.168.14/cdi_demo/index</p><p>用户：guest</p><p>密码：1</p><p><br/></p>','index_page.png');
/*!40000 ALTER TABLE `index_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_info` (
  `MAIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITEM_ID` int(11) DEFAULT NULL,
  `PRODUCT_NAME` varchar(200) DEFAULT NULL,
  `PRODUCT_TITLE` varchar(200) DEFAULT NULL,
  `PRODUCT_INFO` text,
  `PRODUCT_NUMBER` int(11) DEFAULT NULL,
  `PRODUCT_PRICE` float DEFAULT NULL,
  `URL_PATH` varchar(200) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MAIN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_info`
--

LOCK TABLES `product_info` WRITE;
/*!40000 ALTER TABLE `product_info` DISABLE KEYS */;
INSERT INTO `product_info` VALUES (1,1,'洗洁精','雕牌','洗衣',12,12,'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3804910746,1413179037&fm=26&gp=0.jpg','2021-01-08 19:17:57'),(2,1,'香皂','天乐牌','香皂擦香香',200,2,'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2876372797,3619632725&fm=26&gp=0.jpg','2021-01-08 19:18:57'),(3,1,'洗衣液','天乐牌','消毒杀菌',100,35.2,'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3876097152,2130886311&fm=26&gp=0.jpg','2021-01-08 19:19:41'),(4,1,'海报','图片杂志','海报宣传',60,12.3,'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1694957684,1975009504&fm=26&gp=0.jpg','2021-01-08 19:20:03'),(5,1,'餐具','餐盒','塑料制品',500,10,'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1569112767,3073551712&fm=26&gp=0.jpg','2021-01-08 19:20:38'),(6,1,'毛巾','乐天牌纯棉毛巾','纯棉制品',100,3.5,'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=390282547,4128441301&fm=26&gp=0.jpg','2021-01-08 19:20:52'),(7,1,'梳子','小板子精品塑料梳','塑料制品',700,2,'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3078461559,2560019038&fm=26&gp=0.jpg','2021-01-08 19:21:18'),(8,1,'杯子','天鹅咖啡杯','瓷器',80,38.89,'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=494546435,1635270096&fm=26&gp=0.jpg','2021-01-08 19:21:44'),(9,1,'蜡烛','祭祀台蜡烛','圣光蜡烛',90,30,'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=427014531,566402357&fm=26&gp=0.jpg','2021-01-08 19:22:08'),(10,1,'莲子','食品','黑白莲子',1000,0.23,'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3359850033,716377641&fm=26&gp=0.jpg','2021-01-08 19:22:32'),(11,2,'sony相机','α-7','短焦距',100,2899,'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1510108404,1243042716&fm=11&gp=0.jpg','2021-01-08 19:24:16'),(12,2,'佳能相机','数码相机','2',20,14500,'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1301722884,1187355351&fm=11&gp=0.jpg','2021-01-08 19:24:27'),(13,2,'3','3','3',3,3,'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2775171431,4216821154&fm=26&gp=0.jpg','2021-01-08 19:25:05'),(14,2,'4','4','4',4,4,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3379141380,977371511&fm=26&gp=0.jpg','2021-01-08 19:25:15'),(15,2,'5','5','5',5,5,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1914464899,2327540010&fm=26&gp=0.jpg','2021-01-08 19:25:56'),(16,2,'6','6','6',6,6,'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2885593494,2893307644&fm=26&gp=0.jpg','2021-01-08 19:26:43'),(17,2,'7','7','7',7,7,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3317365385,3093427020&fm=15&gp=0.jpg','2021-01-08 19:26:58'),(18,2,'8','8','8',8,8,'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3018753184,1846708116&fm=11&gp=0.jpg','2021-01-08 19:27:58'),(19,2,'9','9','9',9,9,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2733216110,1284790319&fm=26&gp=0.jpg','2021-01-08 19:28:20'),(20,2,'0','0','0',0,0,'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2182628344,1825172978&fm=15&gp=0.jpg','2021-01-08 19:29:00'),(21,3,'1','1','1',1,1,'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1033437980,2362790010&fm=26&gp=0.jpg','2021-01-08 19:29:57'),(22,3,'2','2','2',2,2,'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1782427732,2330149901&fm=26&gp=0.jpg','2021-01-08 19:30:15'),(23,3,'3','3','3',3,3,'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2588355644,3790635696&fm=26&gp=0.jpg','2021-01-08 19:30:56'),(24,3,'4','4','4',4,4,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3333583816,4222977373&fm=26&gp=0.jpg','2021-01-08 19:31:28'),(25,3,'5','5','5',5,5,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2887279438,815621319&fm=26&gp=0.jpg','2021-01-08 19:32:52'),(26,3,'6','6','6',6,6,'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2494083708,1902959334&fm=26&gp=0.jpg','2021-01-08 19:33:30'),(27,3,'7','7','7',7,7,'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2999337944,2243371147&fm=26&gp=0.jpg','2021-01-08 19:34:08'),(28,3,'8','8','8',8,8,'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2630612505,488271841&fm=26&gp=0.jpg','2021-01-08 19:34:21'),(29,3,'9','9','9',9,9,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1816464898,725120931&fm=26&gp=0.jpg','2021-01-08 19:35:45'),(30,3,'0','0','0',0,0,'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1468994868,3878982494&fm=26&gp=0.jpg','2021-01-08 19:36:01'),(31,4,'1','1','1',1,1,'https://ns-strategy.cdn.bcebos.com/ns-strategy/upload/fc_big_pic/part-00499-2480.jpg','2021-01-08 19:36:41'),(32,4,'2','2','2',2,2,'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3352160236,228182337&fm=26&gp=0.jpg','2021-01-08 19:37:05'),(33,4,'3','3','3',3,3,'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3392881855,3365707805&fm=26&gp=0.jpg','2021-01-08 19:38:08'),(34,4,'4','4','4',4,4,'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1960129768,2467501064&fm=26&gp=0.jpg','2021-01-08 19:38:19'),(35,4,'5','5','5',5,5,'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=174525636,4157124350&fm=26&gp=0.jpg','2021-01-08 19:39:13');
/*!40000 ALTER TABLE `product_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_item`
--

DROP TABLE IF EXISTS `product_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_item` (
  `MAIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITEM_NAME` varchar(50) DEFAULT NULL,
  `ITEM_DESC` varchar(200) DEFAULT NULL,
  `URL_PATH` varchar(200) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MAIN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_item`
--

LOCK TABLES `product_item` WRITE;
/*!40000 ALTER TABLE `product_item` DISABLE KEYS */;
INSERT INTO `product_item` VALUES (1,'日用品','日用品','http://124.71.168.14/cdi_CenterData/cdi_app/img_01/content/icon1.png','2021-01-08 14:32:06'),(2,'数码','数码产品','http://124.71.168.14/cdi_CenterData/cdi_app/img_01/content/icon2.png','2021-01-08 14:32:58'),(3,'汽车','汽车用品','http://124.71.168.14/cdi_CenterData/cdi_app/img_01/content/icon3.png','2021-01-08 14:33:14'),(4,'建筑','建筑材料','http://124.71.168.14/cdi_CenterData/cdi_app/img_01/content/icon4.png','2021-01-08 14:33:40'),(5,'中文','测试信息',NULL,'2021-04-22 22:00:03');
/*!40000 ALTER TABLE `product_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_list`
--

DROP TABLE IF EXISTS `seller_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller_list` (
  `MAIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) DEFAULT NULL,
  `BUYER_USER_NAME` varchar(200) DEFAULT NULL,
  `SELLER_PRICE` float DEFAULT NULL,
  `SELLER_NUMBER` int(11) DEFAULT NULL,
  `SUB_VALUE` float DEFAULT NULL,
  `SELLER_UNIT` varchar(200) DEFAULT NULL,
  `URL_PATH` varchar(200) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MAIN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_list`
--

LOCK TABLES `seller_list` WRITE;
/*!40000 ALTER TABLE `seller_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cdi_biz'
--

--
-- Dumping routines for database 'cdi_biz'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-23 15:25:04
